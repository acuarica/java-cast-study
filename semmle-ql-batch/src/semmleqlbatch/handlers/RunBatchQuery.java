package semmleqlbatch.handlers;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//
import com.semmle.api.k;
import com.semmle.api.r;
import com.semmle.plugin.platform.ui.db.ICurrentDBService;
import com.semmle.plugin.platform.ui.preferences.d;
import com.semmle.plugin.platform.ui.util.c;
import com.semmle.plugin.platform.ui.util.c.a;
import com.semmle.plugin.qdt.core.model.b;
import com.semmle.plugin.qdt.ui.QDTUIActivator;
import com.semmle.plugin.qdt.ui.editors.ql.B;
import com.semmle.plugin.qdt.ui.editors.ql.F;
import com.semmle.plugin.report.ui.util.g;
import com.semmle.util.files.h;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RunBatchQuery extends AbstractHandler {
    private static final Logger a = LoggerFactory.getLogger(RunBatchQuery.class);

    public RunBatchQuery() {
    }


    public static <T> List<T> ga(ExecutionEvent event, Class<T> cls) {
        ISelection selection = HandlerUtil.getCurrentSelection(event);
        return ca(selection, cls);
    }

    public static <T> List<T> ca(ISelection selection, Class<T> cls) {
        List<T> selectedTs = new ArrayList();
        if (selection instanceof IStructuredSelection) {
            Object[] var3 = ((IStructuredSelection)selection).toArray();
            int var4 = var3.length;

            for(int var5 = 0; var5 < var4; ++var5) {
                Object element = var3[var5];
                T adaptedElement = a(element, cls);
                if (adaptedElement != null) {
                    selectedTs.add(adaptedElement);
                }
            }
        }

        return selectedTs;
    }


    public static <T> T a(Object obj, Class<T> cls) {
        if (cls.isInstance(obj)) {
            return cls.cast(obj);
        } else {
            if (obj instanceof IAdaptable) {
                Object result = ((IAdaptable)obj).getAdapter(cls);
                if (result != null) {
                    return cls.cast(result);
                }
            }

            return cls.cast(Platform.getAdapterManager().getAdapter(obj, cls));
        }
    }
    
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {

//		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
//		MessageDialog.openInformation(window.getShell(), "Semmle-ql-batch", "Hello, Eclipse world");

        c.a();
        d runPrefs = new d(QDTUIActivator.f().getPreferenceStore());
        if (c.a(event, (B)null, runPrefs).equals(com.semmle.plugin.platform.ui.util.c.a.c)) {
            return null;
        } else {
            ICurrentDBService dbService = (ICurrentDBService)g.b(event, ICurrentDBService.class);
            Set<File> queryFiles = new LinkedHashSet();
            Iterator var5 = ga(event, IResource.class).iterator();

            while(var5.hasNext()) {
                IResource resource = (IResource)var5.next();

                try {
                    resource.accept((resource1) -> {
                        if (resource1 instanceof IFile && com.semmle.plugin.qdt.core.a.a((IFile)resource1)) {
                            queryFiles.add(resource1.getLocation().toFile());
                        }

                        return true;
                    });
                } catch (CoreException var11) {
                    throw new ExecutionException("Exception retrieving query files in selection", var11);
                }
            }

            if (queryFiles.isEmpty()) {
                a.error("No query files in selection");
            }

            if (!c.a(event, queryFiles, runPrefs)) {
                return null;
            } else {
                var5 = queryFiles.iterator();

                while(var5.hasNext()) {
                    File file = (File)var5.next();
                    b context = com.semmle.plugin.platform.ui.db.d.a(file);
                    c.a(context, dbService);

                    try {
                        k files = com.semmle.frontend.imports.a.a(file.toPath(), h.c(file), context.c().a());
                        r queryInput = new F(file.getName(), files);
                        (new com.semmle.plugin.platform.ui.analysis.a(queryInput, context, dbService)).schedule();
                    } catch (IOException var10) {
                        a.error("Exception reading file " + file.getAbsolutePath(), var10);
                    }
                }

                return null;
            }
        }
    }
}
