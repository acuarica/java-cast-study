package semmleqlbatch.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.internal.WorkbenchWindow;

import com.semmle.plugin.platform.ui.handlers.RunQueriesContainedHandler;

import org.eclipse.jface.dialogs.MessageDialog;

public class SampleHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
//		IHandlerService handlerService = HandlerUtil.getActiveWorkbenchWindow(event).getService(IHandlerService.class);
//		try {
//			handlerService.executeCommand("com.semmle.plugin.platform.ui.handlers.RunQueriesContainedHandler", null);
//		} catch (Exception ex) {
//			throw new RuntimeException(ex);
//			// Give message
//		}

//		super.execute(event);

		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		MessageDialog.openInformation(window.getShell(), "Semmle-ql-batch", "Hello, Eclipse world");
		return null;
	}
}
