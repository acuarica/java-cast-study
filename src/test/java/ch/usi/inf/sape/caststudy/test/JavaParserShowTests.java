package ch.usi.inf.sape.caststudy.test;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.printer.JsonPrinter;
import com.github.javaparser.printer.SourcePrinter;
import com.github.javaparser.printer.XmlPrinter;
import org.json.JSONObject;
import org.junit.Test;

import static java.lang.System.out;

public class JavaParserShowTests {

    private static final String CLASS =
            "class A {\n" +
                    "public int m() {" +
                    "  return (int)1L;" +
                    "}" +
                    "public int n() {" +
                    "  this.m();" +
                    "  return new A().m();" +
                    "}" +
                    "}";

    @Test
    public void showCompilationUnitToJsonTest() {
        CompilationUnit compilationUnit = JavaParser.parse(CLASS);

        String t = new JsonPrinter(true).output(compilationUnit);
        out.println(t);

        String s = new JsonPrinter(false).output(compilationUnit);
        JSONObject json = new JSONObject(s);
        out.println(json.toString(2));
    }

    @Test
    public void showCompilationUnitToXmlTest() {
        CompilationUnit compilationUnit = JavaParser.parse(CLASS);

        String s = new XmlPrinter(true).output(compilationUnit);
        out.println(s);
    }

}
