package ch.usi.inf.sape.caststudy.test;

import ch.usi.inf.sape.caststudy.CastBytecodeMain;
import ch.usi.inf.sape.caststudy.CastJavaMain;
import ch.usi.inf.sape.caststudy.CastJavaVsClassMain;
import ch.usi.inf.sape.caststudy.HowManyArtifactsMain;
import javassist.NotFoundException;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;

public class TasksTest {

    private static String MAVENINDEX = "--mavenindex=out/mavenindex.sqlite3";
    private static String REPO = "--repo=cache/repo";

    private static String POPULAR = "'com.loopj.android','cz.msebera.android'";

    private static String ARTS_ALL = "select max(idate), * from artifact_jar group by groupid, artifactid";
    private static String ARTS_POPULAR = "select max(idate), * from artifact_view where groupid in (" + POPULAR + ") and classifier='sources' and packaging = 'jar' and extension = 'jar' group by groupid, artifactid";

    @Test
    public void howManyArtifactsMainPopularTask() throws IllegalAccessException, SQLException, IOException {
        HowManyArtifactsMain.main(new String[]{MAVENINDEX, "--query=" + ARTS_POPULAR});
    }

    @Test
    public void howManyArtifactsMainAllTask() throws IllegalAccessException, SQLException, IOException {
        HowManyArtifactsMain.main(new String[]{MAVENINDEX, "--query=" + ARTS_ALL});
    }

    @Test
    public void castJavaVsClassMainPopularTask() throws IllegalAccessException, SQLException, IOException {
        CastJavaVsClassMain.main(new String[]{MAVENINDEX, REPO, "--query=" + ARTS_POPULAR});
    }

    @Test
    public void castJavaVsClassMainAllTask() throws IllegalAccessException, SQLException, IOException {
        CastJavaVsClassMain.main(new String[]{MAVENINDEX, REPO, "--query=" + ARTS_ALL});
    }

    @Test
    public void castBytecodePopular() throws IllegalAccessException, SQLException, IOException, NotFoundException {
        CastBytecodeMain.main(new String[]{MAVENINDEX, REPO, "--query=" + ARTS_POPULAR});
    }

    @Test
    public void castJavaAll() throws IllegalAccessException, SQLException, IOException, NotFoundException {
//        CastJavaMain.main(new String[]{MAVENINDEX, REPO, "--query=" + ARTS_ALL});
    }

    @Test
    public void castJavaPopular() throws IllegalAccessException, SQLException, IOException, NotFoundException {
//        CastJavaMain.main(new String[]{MAVENINDEX, REPO, "--query=" + ARTS_POPULAR});
    }

}
