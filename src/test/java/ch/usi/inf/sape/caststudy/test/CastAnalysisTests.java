package ch.usi.inf.sape.caststudy.test;

import ch.usi.inf.sape.caststudy.JarBytecodeProvider;
import ch.usi.inf.sape.caststudy.LogResultSaver;
import ch.usi.inf.sape.caststudy.detectors.LookupByIdDetector;
import ch.usi.inf.sape.caststudy.typesolvers.JarSourcesTypeSolver;
import com.github.javaparser.JavaParser;
import com.github.javaparser.ParserConfiguration;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JarTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;
import org.jetbrains.java.decompiler.main.ClassesProcessor;
import org.jetbrains.java.decompiler.main.DecompilerContext;
import org.jetbrains.java.decompiler.main.Fernflower;
import org.jetbrains.java.decompiler.main.TextBuffer;
import org.jetbrains.java.decompiler.main.collectors.BytecodeSourceMapper;
import org.jetbrains.java.decompiler.main.collectors.CounterContainer;
import org.jetbrains.java.decompiler.main.collectors.ImportCollector;
import org.jetbrains.java.decompiler.main.decompiler.PrintStreamLogger;
import org.jetbrains.java.decompiler.main.extern.IFernflowerPreferences;
import org.jetbrains.java.decompiler.main.rels.ClassWrapper;
import org.jetbrains.java.decompiler.main.rels.LambdaProcessor;
import org.jetbrains.java.decompiler.main.rels.MethodWrapper;
import org.jetbrains.java.decompiler.struct.IDecompiledData;
import org.jetbrains.java.decompiler.struct.StructClass;
import org.jetbrains.java.decompiler.struct.StructContext;
import org.jetbrains.java.decompiler.struct.lazy.LazyLoader;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarFile;

import static org.jetbrains.java.decompiler.main.extern.IFernflowerLogger.Severity.TRACE;

public class CastAnalysisTests {

    private static class DC implements IDecompiledData {

        @Override
        public String getClassEntryName(StructClass cl, String entryname) {
            System.out.println(entryname);
            return null;
        }

        @Override
        public String getClassContent(StructClass cl) {
            System.out.println(cl);
            return null;
        }

    }

    ;

    public static String getClassContent(ClassesProcessor classesProcessor, StructClass cl) {
        try {
            TextBuffer buffer = new TextBuffer(ClassesProcessor.AVERAGE_CLASS_SIZE);
            buffer.append(DecompilerContext.getProperty(IFernflowerPreferences.BANNER).toString());
            classesProcessor.writeClass(cl, buffer);
            return buffer.toString();
        } catch (Throwable ex) {
            DecompilerContext.getLogger().writeMessage("Class " + cl.qualifiedName + " couldn't be fully decompiled.", ex);
            return null;
        }
    }

    @Test
    public void decompile() {
        PrintStreamLogger logger = new PrintStreamLogger(System.out);
        logger.accepts(TRACE);
        logger.setSeverity(TRACE);

        JarBytecodeProvider provider = new JarBytecodeProvider("jars/android-async-http-1.4.9-sources.jar", logger);
        LogResultSaver saver = new LogResultSaver(logger);
        logger.writeMessage("asdasdfasdfasdff", TRACE);
        Map<String, Object> options = new HashMap<>();
        options.put("dgs", 1);

        DecompilerContext.initContext(options);
        DecompilerContext.setCounterContainer(new CounterContainer());
        DecompilerContext.setLogger(logger);

        StructContext structContext = new StructContext(saver, new DC(), new LazyLoader(provider));
        structContext.addSpace(new File("jars/android-async-http-1.4.9.jar"), true);
//        structContext.addSpace(new File("jars/AsyncHttpClient.class"), true);
        ClassesProcessor classesProcessor = new ClassesProcessor(structContext);

        DecompilerContext.setClassProcessor(classesProcessor);
        DecompilerContext.setStructContext(structContext);

        structContext.getClasses().forEach((s, structClass) -> getClassContent(classesProcessor, structClass));

        classesProcessor.getMapRootClasses().forEach((s, classNode) -> {
            ClassWrapper wrapper = classNode.getWrapper();
            StructClass structClass = wrapper.getClassStruct();
            structClass.getMethods().forEach(structMethod -> {
                MethodWrapper methodWrapper = wrapper.getMethodWrapper(structMethod.getName(), structMethod.getDescriptor());
                System.out.println(methodWrapper);
            });
        });

        structContext.getClasses().forEach((s, structClass) -> {

            String content = getClassContent(classesProcessor, structClass);
//            System.out.println(content);

            structClass.getMethods().forEach(structMethod -> {
                System.out.println(":: " + s + "::" + structMethod.getName());
                System.out.println(structMethod.getInstructionSequence());
//               if (structMethod.containsCode()) {
//                  structMethod.getInstructionSequence().writeCodeToStream();
//               }
            });
        });
    }

    @Test
    public void decompileWithResultSaver() {
        PrintStreamLogger logger = new PrintStreamLogger(System.out);
        logger.accepts(TRACE);
        logger.setSeverity(TRACE);

        JarBytecodeProvider provider = new JarBytecodeProvider("jars/android-async-http-1.4.9-sources.jar", logger);
        LogResultSaver saver = new LogResultSaver(logger);
        logger.writeMessage("asdasdfasdfasdff", TRACE);
        Map<String, Object> options = new HashMap<>();
        options.put("dgs", 1);

        Fernflower fernflower = new Fernflower(provider, saver, options, logger);

        fernflower.getStructContext().addSpace(new File("jars/android-async-http-1.4.9.jar"), true);
        fernflower.getStructContext().addSpace(new File("jars/AsyncHttpClient.class"), true);

        try {
            fernflower.decompileContext();
        } finally {
            fernflower.clearContext();
        }
    }

    @Test
    public void test() throws IOException {
        CombinedTypeSolver solver = new CombinedTypeSolver();
        solver.add(new ReflectionTypeSolver());
        solver.add(new JarTypeSolver("jars/android-2.3.1.jar"));
        solver.add(new JarTypeSolver("jars/httpclient-4.3.6.1.jar"));
//        solver.add(new JarTypeSolver("jars/httpclient-4.5.4.jar"));
        solver.add(new JarSourcesTypeSolver("jars/android-async-http-1.4.9-sources.jar"));

        JavaSymbolSolver javaSymbolSolver = new JavaSymbolSolver(solver);

        ParserConfiguration pc = new ParserConfiguration();
        pc.setSymbolResolver(javaSymbolSolver);
        JavaParser.setStaticConfiguration(pc);

        try (JarFile jarFile = new JarFile("jars/android-async-http-1.4.9-sources.jar")) {
            jarFile.stream().filter(jarEntry -> jarEntry.getName().endsWith(".java")).forEach(jarEntry -> {
                try {
                    CompilationUnit compilationUnit = JavaParser.parse(jarFile.getInputStream(jarEntry));
                    new LookupByIdDetector(solver).analyse(compilationUnit);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
