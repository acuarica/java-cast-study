package ch.usi.inf.sape.caststudy.test;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.CastExpr;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class JavaParserHowToTests {

    @Test
    public void howToGetClassNameTest() {
        CompilationUnit compilationUnit = JavaParser.parse(
                "class A {\n" +
                        " }\n"
        );
        Optional<ClassOrInterfaceDeclaration> classA = compilationUnit.getClassByName("A");
        assertEquals("A", classA.get().getNameAsString());
        assertEquals(1, classA.get().getBegin().get().line);
    }

    @Test
    public void howToGetFieldsNameAndModifiersTest() {
        CompilationUnit compilationUnit = JavaParser.parse(
                "class A {\n" +
                        "public int f;\n" +
                        "public static int C = 0;\n" +
                        "private int g;\n" +
                        "}\n"
        );

        List<FieldDeclaration> fs = compilationUnit.getNodesByType(FieldDeclaration.class);

        assertEquals("f", fs.get(0).getVariables().get(0).getNameAsString());
        assertTrue(fs.get(0).getVariables().get(0).getType().isPrimitiveType());
        assertEquals("INT", fs.get(0).getVariables().get(0).getType().asPrimitiveType().getType().name());
        assertTrue(fs.get(0).isPublic());
        assertTrue(!fs.get(0).isStatic());

        assertEquals("C", fs.get(1).getVariables().get(0).getNameAsString());
        assertTrue(fs.get(1).getVariables().get(0).getType().isPrimitiveType());
        assertEquals("INT", fs.get(1).getVariables().get(0).getType().asPrimitiveType().getType().name());
        assertTrue(fs.get(1).isPublic());
        assertTrue(fs.get(1).isStatic());

        assertEquals("g", fs.get(2).getVariables().get(0).getNameAsString());
        assertTrue(fs.get(2).getVariables().get(0).getType().isPrimitiveType());
        assertEquals("INT", fs.get(2).getVariables().get(0).getType().asPrimitiveType().getType().name());
        assertTrue(fs.get(2).isPrivate());
        assertTrue(!fs.get(2).isStatic());
    }

    @Test
    public void howToGetOuterMethodTest() {
        CompilationUnit compilationUnit = JavaParser.parse(
                "class A {\n" +
                        "public long m() {\n" +
                        "  return (long)1;\n" +
                        "}" +
                        "}\n"
        );

        List<CastExpr> cs = compilationUnit.getNodesByType(CastExpr.class);
        CastExpr c = cs.get(0);
        MethodDeclaration m = c.getAncestorOfType(MethodDeclaration.class).get();
        assertEquals("m", m.getNameAsString());
    }
}
