
import java.nio.file._
import java.nio.file.attribute.BasicFileAttributes
import java.util

import com.github.javaparser.ast.CompilationUnit
import com.github.javaparser.ast.body.MethodDeclaration
import com.github.javaparser.ast.expr.CastExpr
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade
import com.github.javaparser.symbolsolver.resolution.typesolvers.{CombinedTypeSolver, ReflectionTypeSolver}
import com.github.javaparser.{JavaParser, ParseProblemException}

import scala.collection.JavaConverters._

object CastingMain {

  def findCasts(cu: CompilationUnit): Int = {
    val solver = new CombinedTypeSolver();

    solver.add(new ReflectionTypeSolver());
//    solver.add(new JarTypeSolver("jars/android-2.3.1.jar"));
//    solver.add(new JarTypeSolver("jars/httpclient-4.3.6.1.jar"));
    //        solver.add(new JarTypeSolver("jars/httpclient-4.5.4.jar"));
//    solver.add(new JarSourcesTypeSolver("jars/android-async-http-1.4.9-sources.jar"));
//    JavaSymbolSolver javaSymbolSolver = new JavaSymbolSolver(solver);


    val cs = cu.getNodesByType(classOf[CastExpr]).asScala
//    val cs: util.List[CastExpr] = cu.getNodesByType(classOf[CastExpr])
    var i = 0
    for (c <- cs) {
      i += 1
      println(c + " @ ")

      val typeOfTheNode = JavaParserFacade.get(solver).getType(c.getExpression)
      println(typeOfTheNode)

      val m = c.getAncestorOfType(classOf[MethodDeclaration])
//      if (m.isPresent) {
//        print(m.get)
//      }
    }

//    cs.asScala.foreach(_ => i += 1)
    i
  }

  def main(args: Array[String]): Unit = {
    val matcher = FileSystems.getDefault.getPathMatcher("glob:**.java")
    var cc: Int = 0

    val repo = "../java-cast-inspection/cache/android-async-http"
    Files.walkFileTree(Paths.get(repo), new SimpleFileVisitor[Path] {
      override def visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult = {
        if (matcher.matches(file)) {
          try {
            val cu = JavaParser.parse(file)
            val n = findCasts(cu)
            if (n > 0) {
              println(s"$file ($n)")
              cc += n
            }
          } catch {
            case e: ParseProblemException => Console.err.println("Parse ERROR", e)
            case e: Exception => Console.err.println(e)
          }
        }

        FileVisitResult.CONTINUE
      }
    })

    println(cc)
  }

}
