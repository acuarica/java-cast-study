package ch.usi.inf.sape.caststudy.util.args;

/**
 * Exception internally thrown when some argument is misssing.
 * 
 * @author Luis Mastrangelo (luis.mastrangelo@usi.ch)
 *
 */
final class ArgumentMissingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5053354343992245633L;

}
