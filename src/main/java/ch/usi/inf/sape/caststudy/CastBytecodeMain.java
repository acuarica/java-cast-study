package ch.usi.inf.sape.caststudy;

import ch.usi.inf.sape.caststudy.detectors.CastCount;
import ch.usi.inf.sape.caststudy.util.args.Arg;
import ch.usi.inf.sape.caststudy.util.args.ArgsParser;
import ch.usi.inf.sape.caststudy.util.db.Db;
import ch.usi.inf.sape.caststudy.util.log.Log;
import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;
import javassist.bytecode.CodeAttribute;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;

import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import static org.objectweb.asm.Opcodes.ASM5;
import static org.objectweb.asm.Opcodes.CHECKCAST;

public final class CastBytecodeMain {

    private static final Log log = new Log(System.out);

    public static class Args {

        @Arg(key = "mavenindex", name = "Maven Index path", desc = "Specifies the path of the Maven Index DB.")
        public String mavenIndex;

        @Arg(key = "repo", name = "Maven Inode DB path", desc = "Specifies the path of the output db file.")
        public String repo;

        @Arg(key = "query", name = "URI list", desc = "Specifies the output uri list file (*aria2* format).")
        public String query;

    }

    private static long jarCount = 0;
    private static long javaCount = 0;
    private static long javaCastCount = 0;
    private static long classCount = 0;
    private static long classCastCount = 0;

    private static long classCastCountPerClass = 0;

    public static void main(String[] args) throws IllegalAccessException, SQLException, IOException, NotFoundException {
        final Args ar = ArgsParser.parse(args, new Args());

        try (final Db db = new Db(ar.mavenIndex);
             final ResultSet rs = db.select(ar.query)) {

            while (rs.next()) {
                String path = rs.getString("path");
                log.info("%s", path);

                String jarPath = path.replace("-sources", "");
                jarPath = ar.repo + "/" + jarPath;

                ClassPool cp = ClassPool.getDefault();
                cp.insertClassPath(jarPath);

                try (JarFile jarFile = new JarFile(jarPath)) {
                    Enumeration e = jarFile.entries();

                    while (e.hasMoreElements()) {
                        JarEntry entry = (JarEntry) e.nextElement();
                        if (entry != null) {
                            String fileName = entry.getName();
                            if (!entry.isDirectory() && fileName.endsWith(".class") && !fileName.contains("$")) {
//                                InputStream in = jarFile.getInputStream(entry);

                                String className = fileName.replace('/', '.');
                                className = className.replace(".class", "");
                                CtClass cc = cp.get(className);
                                CodeAttribute ca = cc.getMethods()[0].getMethodInfo().getCodeAttribute();

                                System.out.println(cc);
                            }
                        }
                    }
                }

                jarCount++;
            }

            log.info("---- Summary ----");
            log.info("jarCount:       %,d", jarCount);
            log.info("javaCount:      %,d", javaCount);
            log.info("javaCastCount:  %,d", javaCastCount);
            log.info("classCount:     %,d", classCount);
            log.info("classCastCount: %,d", classCastCount);
        }
    }
}
