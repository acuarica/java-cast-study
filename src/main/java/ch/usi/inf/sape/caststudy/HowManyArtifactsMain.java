package ch.usi.inf.sape.caststudy;

import ch.usi.inf.sape.caststudy.util.args.Arg;
import ch.usi.inf.sape.caststudy.util.args.ArgsParser;
import ch.usi.inf.sape.caststudy.util.db.Db;
import ch.usi.inf.sape.caststudy.util.log.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class HowManyArtifactsMain {

    private static final Log log = new Log(System.out);

    public static class Args {

        @Arg(key = "mavenindex", name = "Maven Index path", desc = "Specifies the path of the Maven Index DB.")
        public String mavenIndex;

        @Arg(key = "query", name = "URI list", desc = "Specifies the output uri list file (*aria2* format).")
        public String query;

    }

    public static void main(String[] args) throws IllegalAccessException, SQLException {
        Args ar = ArgsParser.parse(args, new Args());
        int count = 0;

        try (final Db db = new Db(ar.mavenIndex);
             final ResultSet rs = db.select(ar.query)) {

            while (rs.next()) {
                count++;
            }

            log.info("---- Summary ----");
            log.info("count: %,d", count);
        }
    }
}
