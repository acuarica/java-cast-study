package ch.usi.inf.sape.caststudy.detectors;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.expr.CastExpr;
import com.github.javaparser.ast.expr.InstanceOfExpr;

import java.util.List;

public class CastCount {

    public int analyse(CompilationUnit compilationUnit) {
        List<CastExpr> cs = compilationUnit.getNodesByType(CastExpr.class);
        int i = 0;
        for (CastExpr c : cs) {
            if (!c.getType().isPrimitiveType()) {
//                System.out.println(c.getType());
                i++;
            }
        }

        return i;
    }

}
