package ch.usi.inf.sape.caststudy;

import org.jetbrains.java.decompiler.main.extern.IBytecodeProvider;
import org.jetbrains.java.decompiler.main.extern.IFernflowerLogger;
import org.jetbrains.java.decompiler.util.InterpreterUtil;

import java.io.File;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static org.jetbrains.java.decompiler.main.extern.IFernflowerLogger.Severity.INFO;
import static org.jetbrains.java.decompiler.main.extern.IFernflowerLogger.Severity.TRACE;

public class JarBytecodeProvider implements IBytecodeProvider {

    private final String jarFile;
    private final IFernflowerLogger logger;

    public JarBytecodeProvider(String jarFile, IFernflowerLogger logger) {
        this.jarFile = jarFile;
        this.logger = logger;
    }

    @Override
    public byte[] getBytecode(String externalPath, String internalPath) throws IOException {
        logger.writeMessage("getBytecode| externalPath=" + externalPath + ", internalPath=" + internalPath, INFO);

        File file = new File(externalPath);
        if (internalPath == null) {
            return InterpreterUtil.getBytes(file);
        } else {
            try (ZipFile archive = new ZipFile(file)) {
                ZipEntry entry = archive.getEntry(internalPath);
                if (entry == null) throw new IOException("Entry not found: " + internalPath);
                return InterpreterUtil.getBytes(archive, entry);
            }
        }
    }

}
