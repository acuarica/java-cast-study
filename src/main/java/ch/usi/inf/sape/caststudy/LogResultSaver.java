package ch.usi.inf.sape.caststudy;

import org.jetbrains.java.decompiler.main.extern.IFernflowerLogger;
import org.jetbrains.java.decompiler.main.extern.IResultSaver;

import java.util.jar.Manifest;

import static org.jetbrains.java.decompiler.main.extern.IFernflowerLogger.Severity.INFO;

public class LogResultSaver implements IResultSaver {

    private IFernflowerLogger logger;

    public LogResultSaver(IFernflowerLogger logger) {
        this.logger = logger;
    }

    @Override
    public void saveFolder(String path) {
        logger.writeMessage("saveFolder: " + path, INFO);
    }

    @Override
    public void copyFile(String source, String path, String entryName) {
        System.out.println("asdf");
        System.out.println(source);
        System.out.println(path);
        System.out.println(entryName);
    }

    @Override
    public void saveClassFile(String path, String qualifiedName, String entryName, String content, int[] mapping) {
        logger.writeMessage("saveClassFile: " + path, INFO);
        logger.writeMessage("saveClassFile: " + content, INFO);
        logger.writeMessage(content, INFO);
    }

    @Override
    public void createArchive(String path, String archiveName, Manifest manifest) {
        logger.writeMessage("createArchive: " + path, INFO);
    }

    @Override
    public void saveDirEntry(String path, String archiveName, String entryName) {
        logger.writeMessage("saveDirEntry: path=" + path + ", archiveName=" + archiveName + ", entryName=" + entryName, INFO);
    }

    @Override
    public void copyEntry(String source, String path, String archiveName, String entry) {
        logger.writeMessage("copyEntry: " + path, INFO);
    }

    @Override
    public void saveClassEntry(String path, String archiveName, String qualifiedName, String entryName, String content) {
        logger.writeMessage("saveClassEntry: path=" + path, INFO);
        logger.writeMessage("saveClassEntry: content=" + content, INFO);
    }

    @Override
    public void closeArchive(String path, String archiveName) {
        logger.writeMessage("closeEntry: " + path, INFO);
    }
}
