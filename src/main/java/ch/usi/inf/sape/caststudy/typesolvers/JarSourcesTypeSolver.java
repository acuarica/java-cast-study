package ch.usi.inf.sape.caststudy.typesolvers;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.resolution.declarations.ResolvedReferenceTypeDeclaration;
import com.github.javaparser.symbolsolver.javaparser.Navigator;
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade;
import com.github.javaparser.symbolsolver.model.resolution.SymbolReference;
import com.github.javaparser.symbolsolver.model.resolution.TypeSolver;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class JarSourcesTypeSolver implements TypeSolver {

    private JarFile jarFile;

    private TypeSolver parent;

    private Cache<String, Optional<CompilationUnit>> parsedFiles = CacheBuilder.newBuilder().softValues().build();
    private Cache<String, List<CompilationUnit>> parsedDirectories = CacheBuilder.newBuilder().softValues().build();
    private Cache<String, SymbolReference<ResolvedReferenceTypeDeclaration>> foundTypes = CacheBuilder.newBuilder().softValues().build();

    public JarSourcesTypeSolver(String jarPath) throws IOException {
        this.jarFile = new JarFile(jarPath);
    }

    @Override
    public String toString() {
        return "JavaParserTypeSolver{" +
                "jarFile=" + jarFile +
                ", parent=" + parent +
                '}';
    }

    @Override
    public TypeSolver getParent() {
        return parent;
    }

    @Override
    public void setParent(TypeSolver parent) {
        this.parent = parent;
    }

    private Optional<CompilationUnit> parse(JarEntry srcFile) {
        try {
            return parsedFiles.get(srcFile.getName(), () -> {
                Optional<CompilationUnit> cu;
                try {
                    cu = Optional.of(JavaParser.parse(jarFile.getInputStream(srcFile)));
                } catch (FileNotFoundException e) {
                    cu = Optional.empty();
                } catch (RuntimeException e) {
                    throw new RuntimeException("Issue while parsing " + srcFile.getName(), e);
                }
                return cu;
            });
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
//
//    private List<CompilationUnit> parseDirectory(File srcDirectory) {
//        try {
//            return parsedDirectories.get(srcDirectory.getAbsolutePath(), () -> {
//                List<CompilationUnit> units = new ArrayList<>();
//                File[] files = srcDirectory.listFiles();
//                if (files != null) {
//                    for (File file : files) {
//                        if (file.getName().toLowerCase().endsWith(".java")) {
//                            Optional<CompilationUnit> unit = parse(file);
//                            if (unit.isPresent()) {
//                                units.add(unit.get());
//                            }
//                        }
//                    }
//                }
//                return units;
//            });
//        } catch (ExecutionException e) {
//            throw new RuntimeException(e);
//        }
//    }

    @Override
    public SymbolReference<ResolvedReferenceTypeDeclaration> tryToSolveType(String name) {
        // TODO support enums
        // TODO support interfaces
        try {
            return foundTypes.get(name, () -> {
                SymbolReference<ResolvedReferenceTypeDeclaration> result = tryToSolveTypeUncached(name);
                if (result.isSolved()) {
                    return SymbolReference.solved(result.getCorrespondingDeclaration());
                }
                return result;
            });
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    private SymbolReference<ResolvedReferenceTypeDeclaration> tryToSolveTypeUncached(String name) {
        String[] nameElements = name.split("\\.");

        for (int i = nameElements.length; i > 0; i--) {
            String filePath = nameElements[0];
            for (int j = 1; j < i; j++) {
                filePath += "/" + nameElements[j];
            }
            filePath += ".java";

            String typeName = "";
            for (int j = i - 1; j < nameElements.length; j++) {
                if (j != i - 1) {
                    typeName += ".";
                }
                typeName += nameElements[j];
            }

            System.out.println(String.format("---- %s, %s, %s", name, filePath, typeName));
            JarEntry jarEntry = jarFile.getJarEntry(filePath);
            if (jarEntry != null) {
                Optional<CompilationUnit> compilationUnit = parse(jarEntry);
                if (compilationUnit.isPresent()) {
                    Optional<com.github.javaparser.ast.body.TypeDeclaration<?>> astTypeDeclaration = Navigator.findType(compilationUnit.get(), typeName);
                    if (astTypeDeclaration.isPresent()) {
                        return SymbolReference.solved(JavaParserFacade.get(this).getTypeDeclaration(astTypeDeclaration.get()));
                    }
                }
            }

//            {
//                List<CompilationUnit> compilationUnits = parseDirectory(srcFile.getParentFile());
//                for (CompilationUnit compilationUnit : compilationUnits) {
//                    Optional<com.github.javaparser.ast.body.TypeDeclaration<?>> astTypeDeclaration = Navigator.findType(compilationUnit, typeName);
//                    if (astTypeDeclaration.isPresent()) {
//                        return SymbolReference.solved(JavaParserFacade.get(this).getTypeDeclaration(astTypeDeclaration.get()));
//                    }
//                }
//            }
        }

        return SymbolReference.unsolved(ResolvedReferenceTypeDeclaration.class);
    }
}
