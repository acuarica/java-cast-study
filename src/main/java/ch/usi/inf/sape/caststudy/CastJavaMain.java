package ch.usi.inf.sape.caststudy;

import ch.usi.inf.sape.caststudy.detectors.CastCount;
import ch.usi.inf.sape.caststudy.detectors.LookupByIdDetector;
import ch.usi.inf.sape.caststudy.util.args.Arg;
import ch.usi.inf.sape.caststudy.util.args.ArgsParser;
import ch.usi.inf.sape.caststudy.util.db.Db;
import ch.usi.inf.sape.caststudy.util.log.Log;
import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.semmle.cli.BatchQL;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;

import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import static org.objectweb.asm.Opcodes.ASM5;
import static org.objectweb.asm.Opcodes.CHECKCAST;

public final class CastJavaMain {

    private static final Log log = new Log(System.out);

    public static class Args {

        @Arg(key = "mavenindex", name = "Maven Index path", desc = "Specifies the path of the Maven Index DB.")
        public String mavenIndex;

        @Arg(key = "repo", name = "Maven Inode DB path", desc = "Specifies the path of the output db file.")
        public String repo;

        @Arg(key = "query", name = "URI list", desc = "Specifies the output uri list file (*aria2* format).")
        public String query;

    }

    private static long jarCount = 0;
    private static long javaCount = 0;
    private static long javaCastCount = 0;
    private static long classCount = 0;
    private static long classCastCount = 0;

    private static long classCastCountPerClass = 0;

    public interface Eq<T> {

        boolean eq(T t);

    }

    public static class Point implements Eq<Point> {

        int x, y;

        @Override
        public boolean eq(Point that) {
            return x == that.x && y == that.y;
        }
    }

    public static class Point3d implements Eq<Point3d> {

        int x, y, z;

        @Override
        public boolean eq(Point3d that) {
            return x == that.x && y == that.y && z == that.z;
        }
    }

//    <T> boolean contains(List<Eq<T>> ls, Eq<T> e) {
//        for (Eq<T> x : ls ) {
//            if (x.eq())
//
//        }
//
//        return false;
//
//    }

    public static void main(String[] args) {
        BatchQL.main(new String[]{
               "--compilationCache" , "value",
                "--queryFile","value",
                "--dbscheme","value",
        "--debug", "value",
        "--dilFile","value ",
        "--import"," value",
        "--importList"," value ",
        "--outOfHeapRam","1000",
        "--outputFile"," -o value ",
        "--runQlo"," value ",
        "--saveQlo"," value ",
        "--tmpdir"," -t value ",
        "--tracing"," value... ",
        "--trapRelation"," -t value ",
        "--tupleTransformer"," value... ",
        "--valuesFrom"," v "

        });
    }

    public static void main64(String[] args) throws InterruptedException, FileNotFoundException {
        File initialFile = new File("input.test");
        InputStream is = new FileInputStream(initialFile);
        System.setIn(is);
        com.semmle.api.server.CombinedServer.main(new String[]{"--log-to-stderr", "--debug"});
    }

    public static void main32(String[] args) throws IllegalAccessException, SQLException, IOException {

        final Args ar = ArgsParser.parse(args, new Args());

        try (final Db db = new Db(ar.mavenIndex);
             final ResultSet rs = db.select(ar.query)) {

            while (rs.next()) {
                String path = rs.getString("path");

                log.info("%s", path);

                try (JarFile jarFile = new JarFile(ar.repo + "/" + path)) {
                    Enumeration e = jarFile.entries();

                    while (e.hasMoreElements()) {
                        JarEntry entry = (JarEntry) e.nextElement();
                        if (entry != null) {
                            String fileName = entry.getName();
                            if (!entry.isDirectory() && fileName.endsWith(".java")) {
                                InputStream in = jarFile.getInputStream(entry);
                                CompilationUnit compilationUnit = JavaParser.parse(in);
                                javaCount++;
//                                LookupByIdDetector ca = new LookupByIdDetector();
//                                ca.analyse(compilationUnit);
                            }
                        }
                    }
                }

                jarCount++;
            }

            log.info("---- Summary ----");
            log.info("jarCount:       %,d", jarCount);
            log.info("javaCount:      %,d", javaCount);
            log.info("javaCastCount:  %,d", javaCastCount);
            log.info("classCount:     %,d", classCount);
            log.info("classCastCount: %,d", classCastCount);
        }
    }
}
