package ch.usi.inf.sape.caststudy;

import ch.usi.inf.sape.caststudy.detectors.CastCount;
import ch.usi.inf.sape.caststudy.util.args.Arg;
import ch.usi.inf.sape.caststudy.util.args.ArgsParser;
import ch.usi.inf.sape.caststudy.util.db.Db;
import ch.usi.inf.sape.caststudy.util.log.Log;
import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.tree.*;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.jar.JarFile;

import static java.lang.System.out;
import static org.objectweb.asm.Opcodes.ASM5;
import static org.objectweb.asm.Opcodes.CHECKCAST;

public final class CastJavaVsClassMain {

    private static final Log log = new Log(out);
    private static long sign;

    public static class Args {

        @Arg(key = "mavenindex", name = "Maven Index path", desc = "Specifies the path of the Maven Index DB.")
        public String mavenIndex;

        @Arg(key = "repo", name = "Maven Inode DB path", desc = "Specifies the path of the output db file.")
        public String repo;

        @Arg(key = "query", name = "URI list", desc = "Specifies the output uri list file (*aria2* format).")
        public String query;

    }

    private static long jarCount = 0;
    private static long javaCount = 0;
    private static long javaCastCount = 0;
    private static long classCount = 0;
    private static long classCastCount = 0;
    private static long castbc;

    private static long classCastCountPerClass = 0;

    public static void main(String[] args) throws IllegalAccessException, SQLException, IOException {
        final Args ar = ArgsParser.parse(args, new Args());

        try (final Db db = new Db(ar.mavenIndex);
             final ResultSet rs = db.select(ar.query);
             Db dd = new Db("hola.sqlite3")
        ) {
            byte[] buffer = new byte[8192];

            PrintStream ps = new PrintStream("hash.txt");

            while (rs.next()) {
                String path = rs.getString("path");

                log.info("%s", path);

                try (JarFile jarFile = new JarFile(ar.repo + "/" + path)) {
                    jarFile.stream().filter(jarEntry -> jarEntry.getName().endsWith(".java")).forEach(jarEntry -> {
                        try {
                            MessageDigest md = MessageDigest.getInstance("MD5");
                            InputStream in = jarFile.getInputStream(jarEntry);
                            DigestInputStream dis = new DigestInputStream(in, md);
                            while (dis.read(buffer) != -1);
                            String md5 = DatatypeConverter.printHexBinary(md.digest());
                            ps.println(md5);


//                            CompilationUnit compilationUnit = JavaParser.parse(in);
//                            javaCount++;
//                            CastCount ca = new CastCount();
//                            int i = ca.analyse(compilationUnit);
//                            javaCastCount += i;
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        }
                    });
                }

                String jarPath = path.replace("-sources", "");

                try (JarFile jarFile = new JarFile(ar.repo + "/" + jarPath)) {
                    jarFile.stream().filter(jarEntry -> jarEntry.getName().endsWith(".class")).forEach(jarEntry -> {
                        try {
                            InputStream in = jarFile.getInputStream(jarEntry);
                            ClassReader cr = new ClassReader(in);
                            classCount++;

                            ClassVisitor v = new ClassVisitor(ASM5) {
                                @Override
                                public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
                                    return new MethodVisitor(ASM5) {

                                        @Override
                                        public void visitLocalVariable(String name, String desc, String signature, Label start, Label end, int index) {
                                            super.visitLocalVariable(name, desc, signature, start, end, index);
                                        }

                                        @Override
                                        public void visitTypeInsn(int opcode, String type) {
                                            if (opcode == CHECKCAST) {
                                                classCastCountPerClass++;
                                            }
                                        }
                                    };
                                }
                            };

                            classCastCountPerClass = 0;
                            cr.accept(v, 0);
                            classCastCount += classCastCountPerClass;

                            ClassNode cn = new ClassNode(ASM5);
                            cr.accept(cn, 0);

                            for (MethodNode mn : (List<MethodNode>) cn.methods) {

                                AbstractInsnNode n = mn.instructions.getFirst();
                                while (n != null) {
                                    if (n instanceof TypeInsnNode) {
                                        TypeInsnNode ti = (TypeInsnNode) n;
                                        if (ti.getOpcode() == CHECKCAST) {
                                            castbc++;
                                        }
                                    }
                                    n = n.getNext();
                                }

                                if (mn.localVariables != null) {
                                    mn.localVariables.stream().filter(o -> o instanceof LocalVariableNode).forEach(o -> {
                                        LocalVariableNode l = (LocalVariableNode) o;
                                        if (l.signature != null) {
                                            out.print(l.name);
                                            out.print(", ");
                                            out.print(l.desc);
                                            out.print(", ");
                                            out.print(l.signature);
                                            out.println();
                                            sign++;
                                        }
                                    });
                                }
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                }

                jarCount++;
            }

            log.info("---- Summary ----");
            log.info("jarCount:       %,d", jarCount);
            log.info("javaCount:      %,d", javaCount);
            log.info("javaCastCount:  %,d", javaCastCount);
            log.info("classCount:     %,d", classCount);
            log.info("classCastCount: %,d", classCastCount);
            log.info("castbc: %,d", castbc);
            log.info("sign: %,d", sign);
        }
    }
}
