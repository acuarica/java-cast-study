package ch.usi.inf.sape.caststudy.detectors;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.resolution.declarations.ResolvedFieldDeclaration;
import com.github.javaparser.resolution.declarations.ResolvedMethodDeclaration;
import com.github.javaparser.resolution.declarations.ResolvedReferenceTypeDeclaration;
import com.github.javaparser.resolution.declarations.ResolvedValueDeclaration;
import com.github.javaparser.resolution.types.ResolvedType;
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade;
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFactory;
import com.github.javaparser.symbolsolver.javaparsermodel.contexts.FieldAccessContext;
import com.github.javaparser.symbolsolver.model.resolution.SymbolReference;
import com.github.javaparser.symbolsolver.model.resolution.TypeSolver;
import com.github.javaparser.symbolsolver.model.resolution.Value;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.lang.System.out;

public final class LookupByIdDetector {

    private TypeSolver solver = new CombinedTypeSolver();

    public LookupByIdDetector(TypeSolver solver) {
        this.solver = solver;
    }

    static int n = 1;

    public void analyse(CompilationUnit compilationUnit) {
        List<CastExpr> cs = compilationUnit.getNodesByType(CastExpr.class);
        for (CastExpr c : cs) {
            System.out.println("*" + n + "*");
            n++;
            doCast(c);
        }

        List<InstanceOfExpr> is = compilationUnit.getNodesByType(InstanceOfExpr.class);
        for (InstanceOfExpr i : is) {
            out.println(i);
        }
    }

    private void doCast(CastExpr c) {
        out.println(c);

//        Expression expr = c.getExpression();
//        expr.isMethodCallExpr()
        Optional<MethodDeclaration> mm = c.getAncestorOfType(MethodDeclaration.class);
        if (!mm.isPresent()) {
            return;
        }

        MethodDeclaration m = mm.get();
        out.println(m);

        ResolvedType typeOfTheNode = JavaParserFacade.get(solver).getType(c.getExpression());
        out.println(c.getExpression() + " :: " + typeOfTheNode);

//        out.println(c.getType());
//        out.println(c.getExpression());
        if (c.getExpression().isMethodCallExpr()) {
            MethodCallExpr mce = c.getExpression().asMethodCallExpr();
            SymbolReference<ResolvedMethodDeclaration> methodDecl = JavaParserFacade.get(solver).solve(mce);
            out.println("symbol: " + methodDecl);
            boolean b = isLookupDecl(methodDecl.getCorrespondingDeclaration());
            out.println("islook:" + b);
            out.println("name: " + mce.getNameAsString());
            Optional<Expression> scope = mce.getScope();
            if (scope.isPresent()) {
                ResolvedType t = JavaParserFacade.get(solver).getType(scope.get());
                out.println(scope.get() + " :: " + t);
            }

            mce.getArguments().stream().forEach(e -> {
                if (e.isFieldAccessExpr()) {
                    FieldAccessExpr fae = (FieldAccessExpr) e;
                    out.println(fae);
                    out.println(fae.getScope());


//                    ResolvedType i = JavaParserFacade.get(solver).getType( fae.getScope());
                    ResolvedType i = JavaParserFacade.get(solver).getType( fae);
                    out.println(i);

//                    fae.calculateResolvedType();
//                    System.out.println(fae.getScope().calculateResolvedType());

//                    Optional<Value> ff = new FieldAccessContext( fae, solver).solveSymbolAsValue("TARGET_AUTH_STATE",solver);
                }
            });
        }

//        System.exit(1);
    }

    private static final String STRING = "java.lang.String";
    private static final String OBJECT = "java.lang.Object";

    private static boolean isLookupDecl(ResolvedMethodDeclaration methodDecl) {
        out.println(methodDecl.getParam(0).describeType());
        out.println(methodDecl.getReturnType().describe());

        return !methodDecl.isStatic() &&
                !methodDecl.isDefaultMethod() &&
                !methodDecl.isGeneric() &&
                !methodDecl.hasVariadicParameter() &&
                methodDecl.getNumberOfParams() == 1 &&
                STRING.equals(methodDecl.getParam(0).describeType()) &&
                OBJECT.equals(methodDecl.getReturnType().describe());

    }
}
