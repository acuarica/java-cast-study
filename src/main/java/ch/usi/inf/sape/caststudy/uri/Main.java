package ch.usi.inf.sape.caststudy.uri;

import ch.usi.inf.sape.caststudy.util.args.Arg;
import ch.usi.inf.sape.caststudy.util.args.ArgsParser;
import ch.usi.inf.sape.caststudy.util.db.Db;
import ch.usi.inf.sape.caststudy.util.log.Log;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class Main {

	private static final Log log = new Log(System.out);

	public static final class Args {

		@Arg(key = "mavenindex", name = "Maven Index", desc = "Specifies the output uri list file (*aria2* format).")
		public String mavenIndex;

		@Arg(key = "urilist", name = "URI List", desc = "Specifies the output uri list file (*aria2* format).")
		public String uriList;

		@Arg(key = "query", name = "Query Filter", desc = "Specifies the output uri list file (*aria2* format).")
		public String query;

		@Arg(key = "mirrors", name = "Mirrors", desc = "Comma separated list of mirrors.")
		public String[] mirrors;

	}

	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, IOException,
			SQLException {
		Args ar = ArgsParser.parse(args, new Args());

		try (Db db = new Db(ar.mavenIndex); UriList uri = new UriList(ar.uriList, ar.mirrors)) {
			ResultSet rs = db.select(ar.query);

			int n = 0;

			while (rs.next()) {
				String path = rs.getString("path");
				n++;
				uri.emit(path);
			}

			log.info("No. emitted fetch files: %,d", n);
		}
	}
}
